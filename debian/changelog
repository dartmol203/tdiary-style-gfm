tdiary-style-gfm (1.2.0-3) UNRELEASED; urgency=medium

  * Team upload.
  * d/control: bump standards version, no changes needed

 -- Andre Correa <andre.correa.silva203@gmail.com>  Tue, 26 Mar 2024 08:35:53 -0300

tdiary-style-gfm (1.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-rouge and tdiary-core.
    + tdiary-style-gfm: Drop versioned constraint on ruby-rouge in Depends.

  [ HIGUCHI Daisuke (VDR dai) ]
  * eliminate lintian warning: ruby-interpreter-is-deprecated
  * eliminate lintian warning: patch-not-forwarded-upstream
  * eliminate lintian warning: quilt-patch-missing-description
  * eliminate lintian warning: update-debian-copyright
  * set Rules-Requires-Root as no.
  * Bump Standards-Version to 4.6.1

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Fri, 22 Jul 2022 21:03:43 +0900

tdiary-style-gfm (1.2.0-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.

  [ Youhei SASAKI ]
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Bump debhelper compatibility level to 13
  * Use github as upstream
  * New upstream version 1.2.0
  * Add new patch: relax ruby-twitter-text dependencies
  * update depends: ruby-commonmarker

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 08 Jan 2021 19:07:55 +0900

tdiary-style-gfm (0.5.1-1) unstable; urgency=medium

  * New upstream version 0.5.1
  * Refresh patches
  * Bump ruby-rouge version: >= 2.2 (Closes: #873165)
  * Update debhelper >= 10
  * Bump Standard Version: 4.0.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 28 Aug 2017 13:39:58 +0900

tdiary-style-gfm (0.4.1-1) unstable; urgency=medium

  [ Youhei SASAKI ]
  * Initial release (Closes: #844699)

  [ Hideki Yamane ]
  * fix Vcs-*, remove unnecessary ruby- prefix

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 28 Nov 2016 16:34:18 +0900
